#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import forms
from rango.models import Page, Category, UserProfile
from django.contrib.auth.models import User

class UserForm(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput())
	username = forms.CharField(max_length=128, help_text="")

	class Meta:
		model = User
		fields = ('username', 'email', 'password')

class UserProfileForm(forms.ModelForm):

	class Meta:
		model = UserProfile
		fields = ('website', 'picture')

class CategoryForm(forms.ModelForm):
	name = forms.CharField(max_length=128,  widget=forms.TextInput(attrs={'placeholder': 'Ex.: Javascript', 'autofocus': 'autofocus'}))
	views = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
	likes = forms.IntegerField(widget=forms.HiddenInput(), initial=0)

	class Meta:
		model = Category

class PageForm(forms.ModelForm):
	title = forms.CharField(max_length=128, widget=forms.TextInput(attrs={'placeholder': 'Por favor, informe o título da página.', 'autofocus': 'autofocus'}))
	url = forms.URLField(max_length=200, widget=forms.TextInput(attrs={'placeholder': 'Informe a URL. Ex.: http://www.exemplo.com.br'}))
	views = forms.IntegerField(widget=forms.HiddenInput(), initial=0)

	class Meta:
		model = Page
		fields = ('title', 'url', 'views')

	def clean(self):
		cleaned_data = self.cleaned_data
		url = cleaned_data.get('url')

		if url and not url.startswith('http://'):
			url = 'http://' + url
			cleaned_data['url'] = url

		return cleaned_data