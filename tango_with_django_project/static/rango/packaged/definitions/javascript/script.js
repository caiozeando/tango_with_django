$(document)
  .ready(function() {

	$('.errorlist').addClass('ui red pointing below ui label');

    $('.ui.dropdown')
	  .dropdown({
	    on: 'hover'
	  })
	;

	$('.ui.message').on('click', function() {
	  $(this).toggle();
	});

	$('.ui.label').click(function() {
	  $('.ui.table').show();
	});

  })
;